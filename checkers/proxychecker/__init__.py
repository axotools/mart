from multiprocessing.dummy import Pool as ThreadPool
import threading
import time
from stuff.config_loader import ProxycheckerSettings as PS
from stuff.stuff import stuff, Proxies, pather
import requests
import json
import sys
import socks
proxy_file_http = [x for x in open(stuff.BASIC_PATH + "/proxies_http.txt", encoding="utf8", errors='ignore').readlines() if ":" in x]
proxy_file_socks4 = [x for x in open(stuff.BASIC_PATH + "/proxies_socks4.txt", encoding="utf8", errors='ignore').readlines() if ":" in x]
proxy_file_socks5 = [x for x in open(stuff.BASIC_PATH + "/proxies_socks5.txt", encoding="utf8", errors='ignore').readlines() if ":" in x]

proxy_dic = {"http": proxy_file_http, "socks4": proxy_file_socks4, "socks5": proxy_file_socks5}
proxy_types = ["http", "socks4", "socks5"]




proxy_path = pather("proxies")


def proxy_checker():

    c = 0
    for key in proxy_dic:
        proxies = []
        running = True
        Proxies.proxies_checked = 0
        Proxies.proxies_working_counter = 0
        def status_thread():
            z = 0
            while running:
                z = z + 1
                time.sleep(0.5)
                print(key + " proxies checking status: " + str(Proxies.proxies_checked) + "/" + str(len(proxy_dic[key])) + " Working: " + str(Proxies.proxies_working_counter) + " " + str(z), end="\r")

        t1 = threading.Thread(target=status_thread, args=[])
        t1.daemon = True
        t1.start()

        try:
            myip = str(requests.get(PS.own_ip_getter_url).content.decode())
        except:
            myip = "127.0.0.1"
        def check(x):
            try:
                proxy = proxy_dic[key][x].replace("\n", "")

                if key == "http":
                    proxy_dict = {
                        'http': "http://" + proxy,
                        'https': "https://" + proxy
                    }
                else:
                    proxy_dict = {
                        'http': key + "://" + proxy,
                        'https': key + "://" + proxy
                    }

                r = requests.get(PS.proxy_judge_url, proxies=proxy_dict, timeout=PS.timeout_in_ms / 1000).content.decode()
                rem_add = r.split("REMOTE_ADDR = ")[1].split("REQUEST_SCHEME")[0].replace("\n", "")

                if r.__contains__(myip):
                    transparent = True
                else:
                    transparent = False
            except Exception as e:
                Proxies.proxies_checked = Proxies.proxies_checked + 1
                return
            Proxies.proxies_checked = Proxies.proxies_checked + 1
            number = len(Proxies.proxies_json)
            Proxies.proxies_json[number] = {}
            Proxies.proxies_json[number]["address"] = proxy
            Proxies.proxies_json[number]["transparent"] = transparent
            Proxies.proxies_json[number]["remote_address"] = rem_add
            Proxies.proxies_json[number]["type"] = key
            Proxies.proxies_json[number]["cloudflare_captcha"] = None
            proxies.append(proxy + "\n")
            Proxies.proxies_working_counter = Proxies.proxies_working_counter + 1
            Proxies.proxies_total_working_counter = Proxies.proxies_total_working_counter + 1

        def theads_two(numbers, threads=5):
            pool = ThreadPool(threads)
            results = pool.map(check, numbers)
            pool.close()
            pool.join()
            return results
        if __name__ == "checkers.proxychecker":
            countt = []
            for x in range(len(proxy_dic[key])):
                countt.append(int(x))

            threads_one = theads_two(countt, PS.thread_amount)
        json_data = json.dumps(Proxies.proxies_json, sort_keys=False, indent=4)
        running = False
        time.sleep(0.5)
        print("\nFinished Proxy checking from type " + key)
        open(proxy_path + "proxies_" + key + ".txt", "a").writelines(proxies)
    time.sleep(0.5)
    print("\nFinished Proxy checking")
