import yaml
import os
BASIC_PATH = str(os.getcwd())
config = yaml.load(open(BASIC_PATH + "/config.yml", 'r'))


class CheckerSettings:
    thread_amount = int(config['checker']['i_threads'])
    remove_doubles = bool(config['checker']['b_remove_doubles'])
    result_path = str(config['checker']['s_result_path'])


class ProxycheckerSettings:
    check_proxies = bool(config['proxy_checker']['i_check_proxies'])
    thread_amount = int(config['proxy_checker']['i_threads'])
    timeout_in_ms = int(config['proxy_checker']['i_timeout'])
    remove_doubles = bool(config['proxy_checker']['b_remove_doubles'])
    own_ip_getter_url = str(config['proxy_checker']['s_own_ip_getter'])
    proxy_judge_url = str(config['proxy_checker']['s_proxy_judge'])

